terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "hexlet" {
  name = "macOs-key"
}

# a list of vms
resource "digitalocean_droplet" "web-terraform-vms" {
  count    = length(var.vm_counts)
  image    = "docker-20-04"
  name     = "web-terraform-homework-0${count.index}"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.hexlet.id]
}
resource "digitalocean_loadbalancer" "lb" {
  name   = "loadbalancer-1"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  // pass a collection of vms
  droplet_ids = digitalocean_droplet.web-terraform-vms[*].id
}

resource "digitalocean_domain" "see-hoo-cool-domain" {
  name = "03bite-me.xyz"
  ip_address = digitalocean_loadbalancer.lb.ip
}

// pass a collection of vms
output "droplets_ips" {
  value = digitalocean_droplet.web-terraform-vms[*].ipv4_address
}
