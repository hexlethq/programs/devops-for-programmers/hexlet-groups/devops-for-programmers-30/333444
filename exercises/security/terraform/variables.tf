variable "do_token" {
    description = "DO API token"
    type        = string
}

variable "pvt_key" {
    description = "private DO key"
    type        = string
}

variable "region" {
    description = "Deployment region"
    type        = string
    default     = "ams3"
}