// list of digitalocean_droplet vms
output "webservers" {
  value = digitalocean_droplet.web-vms
}

output "bastion" {
  value = digitalocean_droplet.bastion
}

output "droplets_ips" {
  value = digitalocean_droplet.web-vms[*].ipv4_address
}
