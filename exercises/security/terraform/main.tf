terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_vpc" "vpc" {
  name     = "security-network"
  region   = var.region
  ip_range = "192.168.10.0/24"
}

data "digitalocean_ssh_key" "hexlet" {
  name = "macOs-key"
}

# a list of vms
resource "digitalocean_droplet" "web-vms" {
  count              = 2
  image              = "docker-20-04"
  name               = "security-web-${count.index + 1}"
  region             = var.region
  size               = "s-1vcpu-1gb"
  private_networking = true
  vpc_uuid           = digitalocean_vpc.vpc.id

  ssh_keys = [
    data.digitalocean_ssh_key.hexlet.id
  ]
}

resource "digitalocean_loadbalancer" "lb" {
  name     = "security-lb"
  region   = var.region
  vpc_uuid = digitalocean_vpc.vpc.id

  dynamic "forwarding_rule" {
    for_each = [
      {
        port     = 80
        protocol = "http"
      }
    ]

    content {
      entry_port = forwarding_rule.value["port"]
      entry_protocol = forwarding_rule.value["protocol"]

      target_port     = 5000
      target_protocol = "http"
    }
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  // pass a collection of vms
  droplet_ids = digitalocean_droplet.web-vms[*].id
}

resource "digitalocean_firewall" "web" {
  name = "web-only-lb-and-ssh"

  droplet_ids = digitalocean_droplet.web-vms[*].id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_droplet_ids = [digitalocean_droplet.bastion.id]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "5000"
    source_load_balancer_uids = [digitalocean_loadbalancer.lb.id]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol         = "tcp"
    port_range       = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "53"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_droplet" "bastion" {
  image              = "ubuntu-20-04-x64"
  name               = "security-bastion"
  region             = var.region
  size               = "s-1vcpu-1gb"
  private_networking = true
  vpc_uuid = digitalocean_vpc.vpc.id

  ssh_keys = [
    data.digitalocean_ssh_key.hexlet.id
  ]
}

resource "digitalocean_firewall" "bastion" {
  name = "only-lb-and-ssh"

  droplet_ids = digitalocean_droplet.web-vms[*].id

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol         = "tcp"
    port_range       = "1-65535"
  }

  outbound_rule {
    protocol         = "udp"
    port_range       = "1-65535"
  }

  outbound_rule {
    protocol         = "icmp"
    port_range       = "1-65535"
  }
}