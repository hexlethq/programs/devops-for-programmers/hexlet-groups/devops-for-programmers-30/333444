// pass a collection of vms
output "droplets" {
  value = digitalocean_droplet.web-terraform-vms[*]
}