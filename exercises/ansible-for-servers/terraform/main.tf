terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
  }
}

# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = var.do_token
}

data "digitalocean_ssh_key" "hexlet" {
  name = "macOs-key"
}

# a list of vms
resource "digitalocean_droplet" "web-terraform-vms" {
  count    = length(var.vm_counts)
  image    = "ubuntu-20-10-x64"
  name     = "ansible-for-servers-0${count.index}"
  region   = "ams3"
  size     = "s-1vcpu-1gb"
  ssh_keys = [data.digitalocean_ssh_key.hexlet.id]

  connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }

}
resource "digitalocean_loadbalancer" "lb" {
  name   = "loadbalancer-for-ansible-1"
  region = "ams3"

  forwarding_rule {
    entry_port     = 80
    entry_protocol = "http"

    target_port     = 5000
    target_protocol = "http"
  }

  healthcheck {
    port     = 5000
    protocol = "http"
    path     = "/"
  }

  // pass a collection of vms
  droplet_ids = digitalocean_droplet.web-terraform-vms[*].id
}

resource "digitalocean_domain" "see-hoo-cool-domain" {
  name = "03bite-me.xyz"
  ip_address = digitalocean_loadbalancer.lb.ip
}
